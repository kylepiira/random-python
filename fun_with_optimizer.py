from scipy.optimize import minimize
import numpy as np

def loss(params):
	return np.abs(params[0])

print(minimize(loss, np.array([3])))