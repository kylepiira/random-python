import random
from tqdm import tqdm

chars = '1234567890'
smallest_diff = 10000
numbers = None

for i in range(10000000):
	temp_chars = list(chars)
	combined = ''
	for _ in range(10):
		if len(temp_chars) > 1:
			index = random.randint(0, len(temp_chars) - 1)
		else:
			index = 0
		combined += temp_chars[index]
		del(temp_chars[index])
	num1 = int(combined[:5])
	num2 = int(combined[5:])
	diff = abs(num1 - num2)
	if diff < smallest_diff:
		smallest_diff = diff
		numbers = (num1, num2)
	if i % 100000 == 0:
		print(smallest_diff)
		print(numbers)
		print('\n')

print(smallest_diff)
print(numbers)

