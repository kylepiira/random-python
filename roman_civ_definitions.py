from collections import Counter
from selenium import webdriver
from jinja2 import Template
from tqdm import tqdm
import wikipedia
import random
import json
import time

driver = webdriver.Firefox()

def search(query):
	query = query + ' rome site:en.wikipedia.org'
	driver.get(f'https://duckduckgo.com/?q={query}')
	time.sleep(1)
	results = driver.find_elements_by_class_name('result__a')
	for result in results:
		print(result.get_attribute('href'))
		if 'en.wikipedia.org' in result.get_attribute('href'):
			return result.get_attribute('href')

	return None

if __name__ == '__main__':
	keywords = open('terms.txt').read().splitlines()
	template = Template(open('roman_template.html').read())
	data = []
	for keyword in tqdm(keywords):
		try:
			title = search(keyword).replace('https://en.wikipedia.org/wiki/', '').replace('_', ' ')
			page = wikipedia.WikipediaPage(title)
			links = [l.replace('_', ' ') for l in page.links]
			links = {link: page.content.count(link) for link in links if page.content.count(link) > 1}
			related = [c[0] for c in Counter(links).most_common(10)]
			if title not in related:
				related.insert(0, title)
			data.append({
				'keyword': keyword,
				'related': related,
				'summary': page.summary.split('\n')[0],
			})
		except Exception as e:
			continue

	with open('terms.html', 'w+') as output:
		output.write(template.render(terms=data))
	with open('terms.tsv', 'w+') as output:
		output.write('\n'.join([f'{term["keyword"]}\t{term["summary"]}' for term in data]))

driver.close()