from tqdm import tqdm
import time

t = 10
for _ in tqdm(range(100)):
    time.sleep(t)
    t = t / 2
