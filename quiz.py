x = 0
def T(n):
	global x
	x += 1
	if (n == 0):
		return 1
	return T(int(n/2)) + T(int(n/3))

T(6)
print(x)