class Random:
	def __init__(self, seed):
		self.seed = seed

	def next(self):
		self.seed = round(self.seed**5 % 1)
		return self.seed