﻿import random
import numpy as np
from scipy import optimize

def generate_data_set(size, max=100, min=1):
	return [random.randint(min, max) for _ in range(size)]

def loss(theta):
	maximum = 100
	for _ in range(10):
		bests = []
		for _ in range(1000):
			data = generate_data_set(100, max=maximum, min=1)
			start_looking_index = int(len(data)*theta)
			best_so_far = max(data[:start_looking_index])
			for dp in data[start_looking_index:]:
				if dp >= best_so_far:
					# print('Best: ',dp)
					bests.append(dp)
					break
	l = np.sum((np.array(bests) - maximum)**2)
	print(l)
	return l

print(optimize.minimize(loss, np.array([0.5])))