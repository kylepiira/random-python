import random
from tqdm import tqdm

party_1 = 0
party_2 = 0

for _ in tqdm(range(300000000)):
	choice = random.choice([0, 1])
	if choice == 0:
		party_1 += 1
	else:
		party_2 += 1

print(party_1)
print(party_2)